package com.ibisa.esper;

public class HelloWorldEsperTest {

	public class HelloWorldEvent {
		private String value;

		public HelloWorldEvent(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
	
	
}
