package com.ibisa.process;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestRestController {

	@Autowired
	private RuntimeService runtimeService;

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/throwSignal", method = RequestMethod.POST)
	public void throwingSignal(@RequestBody SignalInfo info) {
		System.out.println("*** Iniciando Servicio ***");
		
		Map<String, Object> processVariables = new HashMap<String, Object>();
		String signalType = info.getSignalType();
		
		for (int i =0; i<=info.getPvi().size()-1; i++) {
			processVariables.put("processInstanceId", info.getPvi().get(i).getProcessInstanceId());
			processVariables.put("activityId", info.getPvi().get(i).getActivityId());
		}

		switch (signalType) {

		case "message":
			System.out.println("** Message **");
			
			runtimeService.startProcessInstanceByMessage(info.getSignalName(), processVariables);
			runtimeService.suspendProcessInstanceById(info.getPvi().get(0).getProcessInstanceId());
			break;

		case "signal":
			System.out.println("** Signal **");
			
			if (!info.getExecutionId().equals(""))
				runtimeService.signalEventReceived(info.getSignalName(), info.getExecutionId(), processVariables);
			else
				runtimeService.signalEventReceived(info.getSignalName(), processVariables);
			break;
		}

	}

}