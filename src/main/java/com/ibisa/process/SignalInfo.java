package com.ibisa.process;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SignalInfo {

	private String signalName;
	private String executionId;
	private String signalType;
	private List<ProcessVariablesInfo> pvi;
	
	public String getSignalType() {
		return signalType;
	}
	public void setSignalType(String signalType) {
		this.signalType = signalType;
	}
	public String getSignalName() {
		return signalName;
	}
	public void setSignalName(String signalName) {
		this.signalName = signalName;
	}
	public List<ProcessVariablesInfo> getPvi() {
		return pvi;
	}
	public void setPvi(List<ProcessVariablesInfo> pvi) {
		this.pvi = pvi;
	}
	public String getExecutionId() {
		return executionId;
	}
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
}
