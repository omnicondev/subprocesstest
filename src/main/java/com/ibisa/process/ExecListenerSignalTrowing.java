package com.ibisa.process;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.runtime.Execution;

import java.util.List;

import org.activiti.engine.RuntimeService;


public class ExecListenerSignalTrowing implements ExecutionListener {
	private static final long serialVersionUID = 1L;

    private RuntimeService runtimeService;  

  
	@Override
	public void notify(DelegateExecution execution) {
//		  String signalProcessInstanceId = (String) execution.getVariable("signalProcessInstanceId");      
//		  System.out.println("signalProcessInstanceId => "+signalProcessInstanceId);
//	      String executionId = runtimeService.createExecutionQuery().processInstanceId(signalProcessInstanceId).signalEventSubscriptionName("exception1").singleResult().getId();
//	      System.out.println("executionId => "+executionId);
//	      runtimeService.signalEventReceived("alert", executionId);
	      //runtimeService.createExecutionQuery().signalEventSubscriptionName("exception1").singleResult();

//		  Execution execution1 = runtimeService.createExecutionQuery().signalEventSubscriptionName("exception1").singleResult();
//		  System.out.println(execution1);
		
		runtimeService.signalEventReceived("exception1");    
	}
}
