package com.ibisa.process;

import org.activiti.engine.delegate.DelegateTask; 
import org.activiti.engine.delegate.Expression; 
import org.activiti.engine.delegate.TaskListener; 
 

/**
 * @author Mariale Morales
 */ 

public class testTaskListener implements TaskListener {
	private static final long serialVersionUID = 1L;
	private Expression workcenter;
	private Expression workunit;

	@Override
	public void notify(DelegateTask delegateTask) {      
		//String workcenter2 = (String) delegateTask.getExecution().getVariable("workcenter1");
		//System.out.println("** Variable Field2 " + workcenter2);
		
		System.out.println("** Variable Field " + delegateTask.getTaskDefinitionKey());
	    System.out.println("** Variable Field3 " + workcenter.getValue(delegateTask.getExecution()));

	    delegateTask.setVariableLocal("workcenter", workcenter.getValue(delegateTask.getExecution()));
	    delegateTask.setVariableLocal("workunit", workunit.getValue(delegateTask.getExecution()));
	  } 
}
