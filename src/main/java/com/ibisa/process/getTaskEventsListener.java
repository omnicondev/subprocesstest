package com.ibisa.process;

import org.activiti.engine.delegate.DelegateHelper;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.delegate.Expression;
import java.util.*;

public class getTaskEventsListener implements TaskListener {

	private static final long serialVersionUID = 1L;
	private Expression workcenter;
	private Expression var1;
	private Expression var2;
	private Expression var3;
	private Expression var4;
	private Expression var5;
	private Expression subprocessVar1;
		
	
	@Override
	public void notify(DelegateTask delegateTask) {
		
		String events = (String) delegateTask.getVariable("events");
		if (events == null) {
			events = delegateTask.getEventName();
		} else {
			events = events + " - " + delegateTask.getEventName();
		}
		
		switch (events) {
		case "create":
			System.out.println("** CREATE **");
			delegateTask.setVariableLocal("subprocessVar1", subprocessVar1.getValue(delegateTask.getExecution()));
			
			break;
		case "complete":
			System.out.println("** COMPLETE **");
			
			break;
		case "delete":
			System.out.println("** DELETE **"); // delete es junto con complete
			break;
		case "assignment":
			System.out.println("** ASSIGMENT **"); // when delegate and resolve
			delegateTask.setVariableLocal("var2", var2.getValue(delegateTask.getExecution()));
			break;
		default:
			System.out.println("** EVENT => " + events + " **");
		}
	}

}
