package com.ibisa.process;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class execListenerSignal implements ExecutionListener {

	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution execution) {
		String processInstanceId = (String) execution.getVariable("processInstanceId");
		
		RuntimeService runtimeService = execution.getEngineServices().getRuntimeService();
		runtimeService.activateProcessInstanceById(processInstanceId);
	}
}
