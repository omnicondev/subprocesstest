package com.ibisa.process.config;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataAccessConfiguration {
	
	@Bean
	public DataSource database() {
	    return DataSourceBuilder.create()
	        .url("jdbc:mysql://localhost:3306/ibisa_process?createDatabaseIfNotExist=true&characterEncoding=UTF-8&autoReconnect=true&useSSL=false")
	        //.username("MM")
	        //.password("MM")
	        .username("root")
	        .password("123456")
	        .driverClassName("com.mysql.jdbc.Driver")
	        .build();
	        
	        
}
	

}
