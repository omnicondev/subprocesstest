package com.ibisa.process;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class getCallActivityNameExecListener implements ExecutionListener {

	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution execution) {
		String events = (String) execution.getVariable("events");
		if (events == null) {
			events = execution.getEventName();
		} else {
			events = events + " - " + execution.getEventName();
		}
		System.out.println("events --> " + events);
		
		System.out.println("Activity ID" + execution.getCurrentActivityId());
		execution.setVariable("activityId", execution.getCurrentActivityId());
	}
}
