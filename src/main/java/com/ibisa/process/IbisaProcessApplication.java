package com.ibisa.process;

import com.ibisa.process.config.*;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.csrf.CsrfFilter;

@SpringBootApplication
public class IbisaProcessApplication {

	public static void main(String[] args) {
		SpringApplication.run(IbisaProcessApplication.class, args);
	}
	
	protected void configure(HttpSecurity http) throws Exception {
	    http.httpBasic().and().authorizeRequests().anyRequest().authenticated()
	        .and().addFilterAfter(new SimpleCORSFilter(), CsrfFilter.class);
	}

}
